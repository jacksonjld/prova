﻿namespace Prova
{
    public class AssertionConcern
    {
        public static void AssertArgumentGreaterThan(decimal value, decimal minimum, string message)
        {
            if (value <= minimum)
            {
                
                throw new DomainException(message);
            }
        }

        public static void AssertArgumentLessThan(DateOnly value, DateOnly maximum, string message)
        {

        }

        public static void AssertArgumentNotEmpty(Guid id, string message)
        {
            if (id.Equals(Guid.Empty))
            {
                throw new DomainException(message);
            }
        }
    }
}